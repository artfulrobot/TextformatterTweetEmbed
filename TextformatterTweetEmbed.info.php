<?php
/**
 * Required properties: title, version, summary. All others are optional.
 *
 */

$info = [
  // title: Module title, typically a little more descriptive than the class name
  'title' => 'Textformatter Tweet Embed',

  // version: The version number, either integer or '1.2.3' string
  'version' => '0.1',

  // summary: A brief description of what this module is
  'summary' => 'Convert links to tweets to embedded tweets in a privacy friendly way',

  // author: Name of the person (or people) that authored this module
  'author' => 'Rich Lott / Artful Robot',

  // Optional URL to more information about the module
  // 'href' => 'https://processwire.com/modules/helloworld/',

  // singular=true: Indicates that only one instance of the module is allowed.
  // This is usually what you want for modules that attach hooks.
  'singular' => true,

  // autoload=true: Indicates the module should be loaded automatically at boot.
  'autoload' => false,

  // Optional font-awesome icon name, minus the 'fa-' part
  'icon' => 'twitter',

  // Optionally describe versions of ProcessWire, PHP or modules that are required.
  // To specify more modules, separate each with a comma (CSV) or use PHP array.
  'requires' => 'ProcessWire>=3.0.165, PHP>=7.4',

  // for more properties that you can include in your module info, see comments
  // the file: /wire/core/Module.php
];



