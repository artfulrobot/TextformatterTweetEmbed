<?php namespace ProcessWire;

/**
 * ProcessWire Tweet Embedding Textformatter
 *
 * Copyright (C) 2023 Rich Lott / Artful Robot
 * Licensed under AGPL3+
 *
 */

class TextformatterTweetEmbed extends Textformatter implements Module {

	const dbTableName = 'textformatter_tweet_embed';
	// const logName = 'textformatter-video-embed';

	/**
	 * Default configuration values
	 *
	 * @var array
	 *
	 */
	protected $configDefaults = [];

	/**
	 * Data as used by the get/set functions
	 *
	 */
	protected $data = [];

	/**
	 * Text formatting function as used by the Textformatter interface
   *
	 * Format the given text string, outside of specific Page or Field context.
	 *
	 * @param string $str String is provided as a reference, so is modified directly (not returned).
	 *
	 *
	 */
	public function format(&$str) {
    // $this->cache->delete('tweet_*');
    $str = preg_replace_callback(';(?:<p>|<div(?: [^>]*)?>)?(https://twitter.com/[a-zA-Z0-9_/-]+/status/\d+)(?!</a>|")\s*(?:</(?:p|div)>);ium', [$this, 'replaceLink'], $str);
	}

  public function replaceLink($matches) {
    list(, $tweetUrl) = $matches;
    $replacement = $this->getTweet($tweetUrl) ?? $tweetUrl;
    return $replacement;
  }

  protected function getCacheKey(string $tweetUrl): string {
    $cacheKey = 'tweet_' . substr(md5($tweetUrl), 0, 12);
    return $cacheKey;
  }

  protected function getTweet(string $tweetUrl): ?string {
    $cacheKey = $this->getCacheKey($tweetUrl);
    $cached = $this->cache($cacheKey, $this->cache->expireNever);
    if ($cached) {
      return $cached;
    }

		$http = new WireHttp();
		$this->wire($http);
    $data = $http->get('https://publish.twitter.com/oembed?url='
      . rawurlencode($tweetUrl));

    if(!$data) {
      // $httpErrorCode = (int) $http->getHttpCode();
      return null;
    }

    $data = json_decode($data, true);
    if (empty($data) || empty($data['html'])) {
      return null;
    }

    // May also want to use url, author_name, author_url
    $html = strip_tags($data['html'], '<blockquote><i><b><em><p><strong><a><div>');

    // Remove tweet tracking/redirects
    $html = preg_replace_callback(';https://t.co/[a-zA-Z0-9_-]+;', [$this, 'removeRedirects'], $html);

    $author = $this->sanitizer->entities($data['author_name']);
    $authorUrl = $this->sanitizer->httpUrl_entities($data['author_url']);
    // @todo make this configurable.
    $html = <<<HTML
      <article class='textfomatter-tweet-embed'>
        <h1><i class='icon-twitter'></i> From
          <a href="$authorUrl" rel="noopener noreferrer" target=_blank
          >$author</a> on Twitter</h1>
        $html
      </article>
      HTML;

    // Cache this.
    $this->cache->set($cacheKey, $html, $this->cache->expireNever);
    return $html;
  }

  public function removeRedirects(array $matches): string {
    $oldUrl = $matches[0];
    $http = new WireHttp();
    $this->wire($http);
    $http->head($oldUrl);
    $newUrl = $http->getResponseHeader('location');
    return $newUrl ?? $oldUrl;
  }

	/**
	 * Upgrade
	 *
	 * @param $fromVersion
	 * @param $toVersion
	 *
	 */
	public function ___upgrade($fromVersion, $toVersion) {
		if($fromVersion || $toVersion) {}
    // If the munging of the html has changed, delete cache.
    // $this->cache->delete('tweet_*');
	}

	/**
	 * Uninstallation routine
	 *
	 */
	public function ___uninstall() {
    $this->cache->delete('tweet_*');
	}

	/**
	 * The following functions are to support the ConfigurableModule interface
	 * since Textformatter does not originate from WireData
	 *
	 * @param string $key
	 * @param mixed $value
	 * @return $this
	 *
	 */
	public function set($key, $value) {
		$this->data[$key] = $value;
		return $this;
	}

	/**
	 * Get configuration item
	 *
	 * @param string $key
	 * @return mixed
	 *
	 */
	public function get($key) {
		$value = $this->wire($key);
		if($value) return $value;
		return isset($this->data[$key]) ? $this->data[$key] : null;
	}

	public function __set($key, $value) {
		$this->set($key, $value);
	}

	public function __get($key) {
		return $this->get($key);
	}
}

