# TextformatterTweetEmbed

ProcessWire text formatter module for embedding a tweet in place of a tweet URL in a privacy friendly way.

## Usage

Install as with other modules.

Then add the TextformatterTweetEmbed to the text formatters list of an HTML field.

When you include a tweet link, e.g.

https://twitter.com/ArtfulRobot/status/1605102861309906944

on a line on its own, it will get transformed to include the oEmbed contents, minus the `<script>` tag. Also `t.co` shortened/tracked links are replaced with their looked-up values.

![screenshot](./screenshot.png)

The wrapping HTML it adds is currently hard-coded at
https://codeberg.org/artfulrobot/TextformatterTweetEmbed/src/branch/main/TextformatterTweetEmbed.module.php#L86

**No styling is included.**
